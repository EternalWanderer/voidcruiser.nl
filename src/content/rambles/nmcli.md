---
title: "Nmcli"
date: "2025-03-06T08:19:36+01:00"
author: "$HUMANOID"
tags: ["NetworkManager"]
description: "An nmcli cheatsheet"
draft: true
---

# Some NMCLI notes.

Recently, I have been spending a lot of time sitting in trains going to work
and back. To be able to get some work done during this time, I have been given
an esim with unlimited mobile data. But this isn't exactly the most reliable
connection as zipping through the landscape at 120 km/h causes me to switch
cell towers quite frequently. As a result, I still prefer to conserve as much
bandwidth as possible. To that end, a colleague pointed out to me that there is
such a thing as a metered connection mode in the NetworkManager configuration.
However, he uses Ubuntu with GNOME and thus has a fancy GUI to manage
NetworkManager for him. I don't. So I dove into `nmcli`. Since I don't use it
all that often, I've decided to write this cheatsheet.
