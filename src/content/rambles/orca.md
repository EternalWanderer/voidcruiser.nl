---
title: "Orca"
date: "2023-09-09T15:59:21+02:00"
author: "$HUMANOID"
tags: ["orca", "audio"]
description: "Rambles on Experiments with Orca"
---

# Brainy Music

There's something I really like about the idea of mathematically and
programmatically making music. As a result, I've harboured an interest in
trackers for quite some time, but never got into attempting to actually use one.
In a similar vein, I've looked at [Hundred Rabbits'
Orca](https://100r.co/site/orca.html) every now and then, but once again
couldn't be bothered to figure it out.

That was until recently, due to unrelated circumstances, I got access to Ableton
Live. This piqued my interest in Orca again and I decided to grit my teeth and
learn the syntax. The result is that I have now produced some blobs of
characters that -- when paired with the right software -- produce some really
interesting noises.

I won't go into detail on how everything works as the documentation is easy
enough to read. The syntax is the biggest hurdle.

# Patterns

My favourite operator so far is `U` -- the eUclidian rhythm operator. It takes a
left and right argument -- lets refer to them as `X` and `Y` respectively -- and
then attempts to produce a bang -- that is to say pulse on `X` out of `Y`
frames. So `3U8` attempts to produce 3 equally spaced bangs in 8 frames, which
will fail. As a result, it spaces the bangs as close to equal as possible. This
can lead to some really interesting patterns and rhythms.

Here's an example of when I wrapped my head around it for the first time:

{{< audio src="/audio/orca/plinkinator.flac" audio=flac >}}

And the Orca code:

```orca
..nVC...........
................
5U9..Vn..4U9..Vn
.*:04C....*:03C.
................
3U5..Vn.........
..:05C..........
................
3U8..Vn.........
.*:06C..........
```

Here's a variant with a different synth and where I've been messing around with
the note being played while recording:

{{< audio src="/audio/orca/varied_plinkinator.flac" audio=flac >}}

# Numbers and stuff

Because I suck at basic calculus and have been half arsedly learning BQN, I
wrote a function to get a list of numbers to produce potentially interesting
patterns for the `U` operator:

```bqn
OrcaList ← {1↕(⊢⋈¨𝕩⊸÷)1+↕𝕩}
```

This function requires a number and returns a list of all possible divisions
from 1 up to the given number.

```bqn
   Orcalist 9
┌─                          
╵ ⟨ 1 9 ⟩                   
  ⟨ 2 4.5 ⟩                 
  ⟨ 3 3 ⟩                   
  ⟨ 4 2.25 ⟩                
  ⟨ 5 1.8 ⟩                 
  ⟨ 6 1.5 ⟩                 
  ⟨ 7 1.2857142857142858 ⟩  
  ⟨ 8 1.125 ⟩               
  ⟨ 9 1 ⟩                   
                           ┘
```

Rough Haskell equivalent for readability's sake:
```haskell
orcaList x = map ((,) <$> id <*> (x/)) [1..x]
```

# Recording from LMMS

Since LMMS puts whatever MIDI notes it gets into a grid (by default at least),
most of the intricacies of timing are lost. As a result, when using LMMS, I use
`pw-record` to record the live audio output with timing still intact.

This audio journey has also lead me to appreciate Pipewire a lot more. In that
regard, I have the following audio stack on NixOS:
```nix
{ config, ... }: {
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
}
```

And I use `qwpgraph` as patchbay.
