---
title: "Xmonad Configuration Bits"
date: "2023-09-13T16:47:48+02:00"
author: "$HUMANOID"
tags: ["xmonad", "haskell"]
description: "A few interesting things I've done with my XMonad config"
draft: true
---

So the other day I was hacking some more on my XMonad config and came across a
few quirks that I don't think I've seen other people implement in their configs.
Though truth be told, I haven't really looked at a lot of other people's config
files.

> "Where is your config file anyway?"

Haven't published it yet as it's still a huge mess of half baked Haskell
snippets.

## Complex terminal sessions

I use [Kitty](https://sw.kovidgoyal.net/kitty/) as my terminal. It's amazing and
probably warrants its own article one day. One of the really cool features is
that it allows you to override options on launch with the `-o` or `--override`
parameter. This allows you to change any of the values in your `kitty.conf`,
this includes the font and colour pallet.

I've been using this to start terminals with
[BQN](https://mlochbaum.github.io/BQN/) compatible font.

```sh
kitty -o font_size=17 -o font_family='BQN386 Unicode' -e bqn
```

This works great and I assigned it to a keybinding for a while. However, some
time later I decided to check out [R](https://www.r-project.org/) and wanted to
have a similar keybinding with slightly different options. Here it dawned on me
that it would be silly to copy-paste the BQN keybinding and change a few things,
so I abstracted that away into a function that takes a list of options and feeds
that to Kitty.

```haskell
runInKittyWithOpts :: [String] -> String -> X ()
runInKittyWithOpts opts executable = spawn $ "kitty "
                                     ++ (unwords . map ("-o " ++)) opts
                                     ++ " -e " ++ executable
```

If I now want to start a kitty session with a BQN font and R repl, I can add
something like the following to a keybinding:
```haskell
runInKittyWithOpts rOptions "R"
    where rOptions = [ "font_size=17"
                     , "font_family='BQN386 Unicode'"
                     ]
```

Since I write quite a bit, I also have a kitty session with a giant margin left
and right of the "terminal" section of the window as that empty space helps me
concentrate. All this is, is the previous function without the `-e` parameter:
```haskell
kittyWithOpts :: [String] -> X ()
kittyWithOpts opts = spawn $ "kitty "
                     ++ (unwords . map ("-o " ++)) opts
```
Though I didn't let that stop me from golfing it down to a point free version:
```haskell
kittyWithOpts :: [String] -> X ()
kittyWithOpts = spawn . ("kitty " ++) . (unwords . map ("-o " ++))
```
My prefered settings for a "text hole" terminal.
```haskell
kittyWithOpts [ "font_family='BQN386 Unicode'"
              , "font_size=14"
              , "single_window_margin_width='5 200'"
              ]
```

If I were to implement this properly, I would make a kitty module with all
options in DataType. But alas, I'm not patient/bored enough for that... yet.

## Mouse bindings

I thought it would be neat to be able to scroll up and down while holding my
modifier key to change my screen brightness and audio levels. The basic concept
here was quite simple; us a lambda function that chucks whatever data it gets
onto the ground because the type signature for mousebindings forces you to deal
with a focussed window.

```haskell
, ((modMask, button4), \_ -> safeSpawn "pulsemixer" ["--change-volume","+1"])
```
