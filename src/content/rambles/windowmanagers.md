---
title: "On window managers and XMonad"
date: "2022-11-03T23:17:35+01:00"
author: "$HUMANOID"
tags: ["linux", "window managers", "xmonad"]
description: "A ramble about, among other things, a highly configurable window manager"
---

# My journey into Tiling Window Managers

When I started my Linux journey, I stuck with GNOME 3 something for around the
first year. Sure, I tried KDE and Cinnamon and XFCE, but GNOME is the one I
always kept coming back to. I think it's because it's pretty much the only one
that felt completely different from what the rest of the world was doing. It
helped me with thinking about Linux as being different from windows. After this
first year though, I came across a few videos about tiling window managers and
wanted to try one. The first one I installed was [i3](https://i3wm.org/).

I hated it.

I had a hard time configuring it as I didn't really know what I was doing.
Other than that, I found it plain awkward to use. The way it tiles windows --
and how you _still_ have to babysit pretty much every last one of them --
drives me away from i3 to this day. I'm sure it has the potential to be a great
window manager. It's just _really_ not for me. Which is a shame, I would really
like to properly live in Wayland with [Sway](https://swaywm.org/ "Sway is like
i3 in Wayland") for at least a little while.

After a few hours of trying to get things to work in i3, I went back to GNOME.
Some time later, I came across [AwesomeWM](https://awesomewm.org/). It was
being recommended as a fairly easy window manager to start with -- it having
window decorations and it's own menu system. The first thing I did was try-and
fail to rip out said menu system and window decorations. I was putting too much
on myself trying to learn the basics of both using a tiling window manager and
Lua.

Some time after that, I came across
[BSPWM](https://github.com/baskerville/bspwm). This was the first time where I
felt like I _really_ managed to get a tiling window manger to do what I wanted
it to do. For some odd reason, there are people out there who consider BSPWM a
more "advanced" window manger. I really don't get why. To this day, I am of the
opinion that SXHKD's configuration syntax is some of the best out there. I
think it took me around 10 minutes to wrap my head around the basics. Somewhere
around an afternoon later, I had a config that served me well for the next few
months to come.

A while after BSPWM, I decided to give suckless'
[DWM](https://dwm.suckless.org/) a shot. Despite my lack of knowledge of C,
this very quickly became my favorite window manager at the time. There is just
something about the insanity of using diff files to configure your piece of
software when perfectly functional configuration libraries and languages exist
that got it's hooks in me. I also caught the minimalism bug around this time,
so DWM's nearly non-existent memory footprint was also great. Despite this
really being the first really "advanced" window manager, I had an easier time
configuring it than AwesomeWM or i3. It was also the first time where I could
appreciate the master-stack layout properly and not having to think about
keeping track of windows in two dimensions anymore. It made me realise that I
want to have to think as little as possible about window positioning. It was
the reason I couldn't deal with i3's paradigm and shifted away from BSPWM the
moment I found DWM.

I ran DWM as my main window manger for over a year before having issues with
some fullscreen applications and the JetBrains suite, which I had to use for
college activities.

{{< img link="https://xkcd.com/1806/" src="/images/xkcd/borrow_your_laptop.png" >}}

I decided to give AwesomeWM another shot after having figured out what I want
from a tiling window manger. This time I managed to get something that worked
pretty much exactly how I wanted it to. In other words, a fairly basic
configuration with most of the default features ripped out and instead my
partially-organically-grown-probably-batshit-insane-keybindings (I use `Super` +
`Space` to open my run launcher. In fact, on my keyboard I have holding the big
space bar (yes it has two space bars, its layout is about as insane as my
window management keybindings) bound to `Super` + `Space`).

To this day I still use DWM quite frequently on machines where I don't really
want to think about what graphical interface to chuck on it (hence I half
arsedly maintain an Alpine package of my fork).

For quite a long time I used DWM and AwesomeWM depending on whether I was
planning on frequently using fullscreen applications and how strong the machine
in question was; AwesomeWM being noticeably slower than DWM on _really_ old
machines (like RejuvinatedBrick). Until at some point, I came across
[XMonad](https://xmonad.org/).

I tried it for an evening.

I hated the fact that it's configured in _sodding Haskell_ and went back to DWM.

A few months later, I came across an implementation of chorded keys through
the `XMonad.Util.EZConfig` module and decided to give it another shot.

This time I was hooked.

The biggest problem I had with it was _still_ the fact that it was configured
in Haskell, but the absurd level of customisability made it worth dealing with
the functional pain. It was also the first time I decided to not bother with a
status bar as getting a basic configuration going had given me enough grief for
one month. 

As for gripes I had (and partially still have) with XMonad, they were quite few,
surprisingly. Coming from AwesomeWM and DWM, I would've liked XMonad to use tags
instead of workspaces; fullscreen is a bit of a pain to get working, but has
less fuckups than DWM; the fact that it's configured in Haskell; it not having a
set of workspaces per monitor (at least by default, I kind of stopped caring
after a little while); the fact that it's configured in Haskell; the
`XMonad.Layout.ShowWName` module being kind of unstable and last but not least,
the fact that it's configured in _fucking Haskell_.

It almost seems like I got sick of being sick of it being configured in Haskell
and I decided to dive into learning the language with the goal of being able to
fully understand my monstrous 384 line config file (586 lines including the
documentation in commented sections).

{{< img src="/images/config_length.png" >}}

I don't fully understand it yet at the time of writing, but I do understand it
a hell of a lot better than when I produced most of those 300 lines.

The greatest thing about XMonad is simultaneously the thing that kept me away
from it: it's written and configured in bloody Haskell. Thus there is barely a
separation between configuration and source code. The only difference there
_really_ is, is the filename. Once you understand a bit of Haskell (no small
task if you're used to imperative languages) adding and integrating your own
features is really easy. And then they are _properly_ integrated. Sort of like
how DWM works, only with proper documentation and support. Part of me wants to
see if I can figure out a way to package my XMonad build as a single binary to
be able to chuck it onto systems without putting much thought into it.

Weird thing with Haskell I'm noticing so far is that I'm slowly but surely
managing to dig up old concepts that I tried to implement in imperative
languages, but couldn't due to their nature. Things like pattern matching and
maps are quite intuitive to me. It's just that Haskell's syntax takes a _lot_ of
getting used to. In the mean time, I guess that my experience with POSIX Shell
bridged the gap between my knowledge of Go and C#; and Haskell.

I highly recommend giving XMonad a shot if you're willing to bash your head
against the Haskell wall for a while before understanding how your window
manager works.

Looking back I find it quite funny to see how I went from not being able to get
i3 to do what I wanted it too and giving up; to barely being able to write or
understand Haskell but putting up with it.
