---
title: "Home"
date: 2022-08-10T14:19:40+02:00
description: "Digital enclave of the Voidcruiser"
---
# This thing might contain a ramble every now and then

Welcome to my little corner of the internet!

If you're a sane person, you might be wondering why I made this site.
 That is to say, you are the type of person who hasn't seen several dozen sites like this one yet.
To you, dear sane person, the quickest way to explain why I made this site would to to call it an expression of ideology.
Or at least part of it.

Other reasons I made this site include wanting a place to shout into the public consciousness known as the internet without making use of well known/existing platforms.
I don't like mainstream platforms as they restrict me to what big tech wants me to express.
As you might be able to guess from the design of this page, I am not a big fan of big tech and centralisation of power.

I might kick various services into the air at some point if I feel like it (have been thinking about PeerTube and Mastodon or Pleroma not that I really know what I'd do with them).

{{< noscript content="Hello fellow Non-JavaScript-runner! This site page doesn't use any JavaScript unless mentioned." >}}
