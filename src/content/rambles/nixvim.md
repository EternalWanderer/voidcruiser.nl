---
title: "NixVim"
date: "2024-01-15T02:53:23+01:00"
author: "$HUMANOID"
tags: ["nix", "linux", "vim"]
description: "Some thoughts on Vim, NeoVim and NixVim"
draft: true
---

# Vim and Editors in General

So I've been using Vim for nearly as long as I've been using Linux. I still
vaguely remember awkwardly working `vimtutor` for the first time. Before I
touched Vim, I didn't really have any strong preference for an editor on Linux.
I'd just come from Windows where I'd used Notepad++ to edit small text files and
Visual Studio and JetBrains Rider for my college work at the time -- mainly C#
in conjunction with Unity. Sure, these editors got the job done, but they
weren't... Elegant. Visual Studio and Rider are monstrous pieces of software
with several kitchen sinks. And I'm sure Notepad++ has some really cool features
that I've never touched, but then it didn't really feel inviting to me and thus
I never dug deeper. When moving to Linux, I mainly used Gedit to edit the odd
ini file. This got rather tedious when needing root privileges to edit, say an
`sshd_config` file. Sure, you can open Gedit using `sudo`, but that makes the
terminal window from which you start it useless as long as Gedit is running. On
top of that, it being a GTK application means that it spews a lot of output to
`STDOUT`. At this point -- not wanting to dig into Vim yet -- I found out Nano
is a thing. I never liked it. It's always felt something like a clunky
on-graphical Windows Notepad of Linux.

So after a little while I decided to bite the bullet and start up `vimtutor`.
As mentioned before, it was awkward at first. But I could also see the power
that the anatomy of Vim allows the user to wield and decided to commit at least
the basics to muscle memory.

# Plugins and NeoVim

From here it would only be a matter of time.
