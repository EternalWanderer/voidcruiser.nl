---
title: "Aural Insanity"
date: 2022-08-10T15:09:31+02:00
description: "Insanity of the aural variety."
---
# Bid Farewel to your Sanity
## A thing consisting of 4 audio tracks.

The tracks used in this... thing are from the following games:

|Track Source                     | Channel |
|---------------------------------|---------|
|Don\'t Starve OST - Ragtime      | Center  |
|Blood - The Chanting track       | Center  |
|Painkiller - The Carnival level  | Left    |
|Blood - The Mall/Elevator music  | Right   |

This is a file dedicated to aural insanity produced every now and then

{{< audio src=/audio/insanity.wav audio=wav >}}

# Anvil
This is the result of deciding to mess around with LMMS, a distorted Minecraft anvil sound ripped from YouTube and having a friend nearby with knowledge on how to create music (in other words, a musician).

I made an anvil melody of sorts and Vlams added a drum sample loop and a few synthesizers.

{{< audio src=/audio/aaaaanvil.flac audio=flac >}}

# Orca

Some [Orca experiments.](/rambles/orca/)

## plinkinator

{{< start-details summary="Orca source" >}}
```orca
..................
..nVC.............
..................
5U9..Vn..4U9..Vn..
.*:04C....*:03C...
..................
3U5..Vn...........
..:05C............
..................
3U8..Vn...........
.*:06C............
..................
```
{{< end-details >}}

### First result

{{< audio src=/audio/orca/plinkinator.flac audio=flac >}}

### Later variant
{{< audio src="/audio/orca/varied_plinkinator.flac" audio=flac >}}

## Shepard tone

{{< start-details summary="Orca source" >}}
```orca
.C8C7........
.1.67TABCDEFG
.J.3XG.......
.18T1234567..
...2.........
D1.J.........
*:02G.2......
```
{{< end-details >}}

### Climb
{{< audio src="/audio/orca/climb.flac" audio=flac >}}

### Fall

Same as 'climb' but with the note and octave loops reversed.
{{< audio src="/audio/orca/fall.flac" audio=flac >}}
