{ description = "Voidcruiser website";
  inputs = {
    nixpkgs.url = "github:NixOs/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    themes = {
      url = "gitlab:EternalWanderer/themes";
      flake = false;
    };
  };
  outputs = { self, nixpkgs, flake-utils, themes }:
  flake-utils.lib.eachDefaultSystem (system:
  let
    pkgs = import nixpkgs { inherit system; };
  in {
    packages = {
      default = pkgs.stdenvNoCC.mkDerivation rec {

        pname = "voidcruiser";
        version = "1.3";
        src = ./src;

        buildInputs = [ pkgs.hugo ];

        buildPhase = ''
          hugo --themesDir="${themes.outPath}"
        '';

        installPhase = ''
          mkdir -p $out/share/
          cp -r public $out/share/${pname}
        '';

      };
    };
  });
}
