src := public
target := voidcruiser.nl
host := voidDroplet

push: $(target)
	rsync -ahv $(target) $(host):/var/www/ --delete

$(target): build
	mv $(src) $(target)

build: clean
	hugo

clean:
	rm -rf $(src) $(target)

.PHONY := build push clean
