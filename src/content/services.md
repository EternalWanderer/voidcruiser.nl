---
title: Services
---

These are a few serices I provide to the people who may be interested in them.

# [SearXNG instance](/searx)

Since DuckDuckGo decided to censor content a few weeks ago at time of writing, the thought of spinning up another SearX instance started to form in the back of my mind.
Then *at* time of writing, they decided to start to block more content in relation to copyright infringing material.
This still doesn't nessecarily mean too much, but who knows how much they're going to continue blocking now they've started in the first place.

So here's that SearX instance, {{< bold-gay content="enjoy!" >}}

I have got to say that the theming of SearXNG has improved quite a lot either over the years or in my mind.
At the very least I think the default theme looks quite nice these days.

{{< noscript content="SearXNG does make use of JavaScript for certain functions, but it's purely to make using it a smoother experiece and it's perfectly functional without JavaScript." >}}
