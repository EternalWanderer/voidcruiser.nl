---
title: "Distortions"
date: "2024-09-26T17:14:09+02:00"
author: "$HUMANOID"
tags: ["ffmpeg", "imagemagick"]
description: I've been messing around with various tools to create some rather nasty looking and sounding things.
---

# Festering Ideas

{{< start-details summary="Epilepsy warning" >}}
{{< img src="/images/distortions/butchered.gif" alt="A gif with flickering text saying 'butchered'" >}}
{{< end-details >}}

I've always liked glitchy looking images and videos. I just never bothered
creating my own. That is until I came across the
[`frei0r`](https://dyne.org/software/frei0r/) library by the
[Dyne](https://dyne.org) project. Here I found the `glitch0r` effect and messed
around with it for a bit in Kdenlive. However, as a terminal terminal user,
using a graphical interface didn't feel right. So I figured out how to use the
`frei0r` effects with `ffmpeg`.[^1] Now I could script the butchering of a bunch of
files at once!

Among the effects in `frei0r`, I also found `scanline0r` and a bunch of other
effect, but wasn't quite happy with the fact that the scanlines in question seem
to be stuck at one resolution. Some time later (a few years), I heard someone
complain about professional footage being interlaced and it looking like shit as
a result. Upon closer inspection it had been the kind of ugly mess I'd been
hoping to create with the `scanline0r` filter. So I got around to figure out how
to interlace video using `ffmpeg` and butchered some videos further.

## First Manifestations

Here's a demonstration of what I managed to do so far using Big Buck Bunny as an
example:

{{< video src="/videos/distortions/rabbit_original.mp4" type="video/mp4" width="500"
caption="Original clip from Big Buck Bunny">}}

This snippet was chopped up using `ffmpeg` using the following command:
```sh
ffmpeg -ss 00:01:17 -i big_buck_bunny_720p_h264.mov -to 00:00:11.75 rabbit_original.mp4
```

{{< start-details summary="Epilepsy warning" >}}

{{< video src="/videos/distortions/rabbit_mangled.mp4" type="video/mp4" width="500" caption="My butchered version">}}

{{< end-details >}}

```sh
ffmpeg -ss 00:01:17 -i big_buck_bunny_720p_h264.mov -vf 'frei0r=filter_name=glitch0r:filter_params=0.05|0.8|.02|1,rgbashift=rh=6:bh=-4,interlace=scan=tff:lowpass=complex,format=yuva420p' -to 00:00:11.75 rabbit_mangled.mp4
```
The full original video can be found on [the Big Buck Bunny website](https://peach.blender.org/)

# Magick

In the mean time, I had been messing around with imagemagick as well. Using it
to create pictures of words of phrases when I felt like it. Usually I would use
something like the following to that end:
```sh
magick -font $(magick -list font | grep Font: | awk '{print $2}' | fzf) -pointsize 288 -fill white -stroke black -strokewidth 5 -background transparent label:"*Insert text here*" example.png
```
{{< img src="/images/distortions/example.png" caption="The font used is Iosevka" alt="Here I used the Iosevka font to create an image with the text *Insert text here*">}}

# Black Magick

So the other day, I thought it would be interesting to see if I could make some
text using imagemagick and then feed it to `ffmpeg` to create a gif with an
interesting glitch effect.

### Initial text

Some text made with imagemagick because its the easiest way I know to create a
simple image with text.

```sh
magick -font Iosevka -pointsize 300 -fill cyan -background transparent label:"Glitch" glitch.png
```
{{< img src="/images/distortions/glitch.png" alt="An image with cyan text saying 'Glitch'" >}}

> *Note*: The `pointsize` here is relevant as `ffmpeg` will complain if the image
> has a pixel count not divisible by 2. 300 works with Iosevka, if it doesn't
> work with your font of choice, try a few sizes until you find one that does.

### With glitch0r

```sh
ffmpeg -loop 1 -i glitch.png -vf 'frei0r=filter_name=glitch0r:filter_params=0.05|0.8|.02|1' -t 10 glitch.gif
```
{{< img src="/images/distortions/glitch.gif" caption="" alt="The same image as above but with generated/artificial interference using the glitch0r effect from the frei0r library" >}}

### Mangled results with imagemagick

Initial results looked quite interesting. Then I thought it'd be funny to
convert this gif to an apng -- animated png. This resulted in the gif being
butchered in a really interesting way due to something in imagemagick being
broken; when converting the gif to an apng using `ffmpeg`, it will come out on
the other end in a state you'd expect.

{{< start-details summary="Epilepsy warning" >}}
{{< img src="/images/distortions/glitch_butchered.gif" alt="A variant of the same image as before, but horrible broken due to the aforementioned bug in imagemagick" >}}
{{< end-details >}}

```sh
magick glitch.gif APNG:glitch_animated.png
```
> *Note*: You have to supply the fileformat in prefix form as apng isn't the
> standard format for the 'png' file extension.

After creating the apng I decided to convert it back to a gif in order to save
some space as the apng was 11M.

```sh
magick APNG:glitch_animated.png glitch_butchered.gif
```

I experimented a bit with both `ffmpeg` and imagemagick to see what the best
results would be. `ffmpeg` is significantly faster with the default settings but
has a slightly large filesize.

```sh
$ ls -lh glitch_*
-rw------- 1 yog-sothoth users  11M Sep 26 18:57 glitch_animated.png
-rw-r--r-- 1 yog-sothoth users 5,1M Sep 27 09:48 glitch_ffmpeg.gif
-rw-r--r-- 1 yog-sothoth users 4,2M Sep 27 09:49 glitch_magick.gif
```

> *Note*: No, I have no idea why the apng has a umask of `077`. Imagemagick just
> spat it out like that.

# Conclusion

I hadn't seen anyone butcher visuals quite like this before, so I thought I'd
write a post about it with the idea that someone else might find it interesting.

So, go out there and butcher some visuals!

[^1]: For many of the frei0r effects I used Kdenlive to figure out their parameters.
      And for a list of available filters, I checked the `ffmpeg-filters(1)` manpage and
      looked for `frei0r`. Under the `filter_name` section, there are a few paths
      listed where ffmpeg checks for `frei0r` filters.
