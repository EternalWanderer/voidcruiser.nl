---
title: About
description: "The obligatory about page"
---
# The HUMANOID running this thing {#HUMANOID}

## Identity - 's a funny thing isn't it?

{{< about >}}

# The site itself

- Colorscheme: [Gruvbox](https://github.com/morhetz/gruvbox)
Depending on `prefers-color-scheme:` in your browser settings being `light` or not, you get to see the light variant or not.
- This site is viewable and usable on every browser I've thrown at it so far.\
I even spent a 10 minutes making this site look reasonable on mobile devices.\
Here's the browser list:
    - Qutebrowser
    - Bromite
    - LibreWolf
    - FireFox
    - UnGoogled Chromium
    - Lynx
    - Midori
    - w3m
    - Dillo
    - Netsurf
    - Nyxt

# Software used by [$HUMANOID](#HUMANOID)

| category | programs |
|---|---|
| Operating system: | NixOS, Debian, Alpine |
| Window manager: | XMonad
| Graphical browser: | LibreWolf, UnGoogled Chromium, Qutebrowser
| Text browser: | Lynx, w3m
| Gemini client: | Lagrange, Amfora
| Terminal: | Kitty
| Text editors: | Vim, NeoVim, VSCodium with NVim plugin
| Image viewer: | nsxiv
| Video player: | mpv
| Music player: | mpd + ncmpcpp

