---
title: "Nix on Other Distros' Packagemangers"
date: "2022-09-12T11:37:11+02:00"
author: "$HUMANOID"
tags: ["nix", "linux"]
description: "A guide on installing the Nix packagemanger on Alpine and Debian"
toc: true
---

# Introduction

The Nix package manager is an amazing tool that allows you to manage your packages through a purely functional environment.
I'm not going to get into why it's amazing or how it really works in this article.
This is purely a guide to installing it in Alpine and Debian Linux.

> "Why not use the official instructions?"

The official instructions require you to `curl` a script directly into `sh`.
From there it requires sudo privileges to install the package manager itself.
This sets of a load of alarm bells in terms of security and what it's going to do from there.
As the Nix package manager is packaged for both Debian and Alpine, my two favourite distros next to NixOS, there is no real reason _not_ to use their respective package managers.

When you try to install Nix on Alpine using the aforementioned installation script you also get the warning that it only supports systemd and that you're on your own when it comes to getting it to work on any other init system (Alpine uses OpenRC).
This problem is fixed in the Alpine package.

Then there is also the benefit of of being able to remove it with either `apt autoremove --purge nix-setup-systemd` or `apk del nix` instead of having to run said same script a second time.

Yes, I'm aware that you don't _have_ to curl the script directly into `sh` and that you can download it to your local system to see what it's actually doing.
But that doesn't take away from the fact that the official instructions tell you to perform an inherently insecure set of actions by trusting what is effectively a random script on the internet.

> "Doesn't this mixing of packagemanagers cause a huge amount of anomalies?"

No, that's part of the beauty of Nix.
Pretty much everything it does lives in `/nix` and is softlinked into place, meaning it never interferes with your existing package manager.

_*DISCLAIMER*_: Debian Stable at time of writing has Nix version `2.3.1`, which appears to have some issues that would be fixed by moving to a later version.
Using the official instructions would sidestep this.

# Installation Process

## Package Installation

The first step is to install the package itself.
On Debian this is done with:

```sh
sudo apt install nix
```

On Alpine, you first have to add the `testing` repo.
This is part of the `edge` branch.
As far as I know, you _could in theory_ get away with just adding the `testing` repo and not moving the rest of your system over to `edge`, but I highly doubt that will do any good for the stability of your system.

Moving your system over to the `edge` branch is done by opening `/etc/apk/repositories`, uncommenting the mirrors referring to `edge` and commenting out the lines referring to (at time of writing) version `3.16`.

From there, run...
```sh
doas apk -U upgrade
```
...to move your system over to `edge`.

Now installing Nix can be done as usual with:
```sh
doas apk add nix
```

This will install the required binaries and the Nix daemon.

## Starting the Daemon

On Debian the Nix daemon is enabled by default.
To make sure it's running and enabled, run:
```sh
sudo systemctl status nix-daemon.service
```

If for whatever reason it's not started and/or enabled, run:
```sh
sudo systemctl enable --now nix-daemon.service
```

On Alpine, the daemon isn't added to any run level by default.
This is done using:
```sh
doas rc-update add nix-daemon
```
Starting it is then done using:
```sh
doas rc-service nix-daemon start
```

## Groups

Next step is to add your account to the `nix-user` or `nix` group depending whether you are on Alpine or Debian respectively.

On Debian this can be done with:
```sh
sudo usermod -aG nix-user $USER
```

And on Alpine this can be done with:
```sh
doas addgroup $USER nix
```

## Environment variables

The next step is to set add the Nix binary dir to your `PATH`.
To that end, I have the following in my `.profile`
```sh
# nix
export PATH=$PATH:$HOME/.nix-profile/bin
```

Now source your `.profile` again, either using...
```sh
source ~/.profile
```
...or by logging out and back in again.

## Adding a channel

Now you have a functioning Nix package manager configuration and all that remains to be done is adding and syncing the repo.

The Alpine package defaults to adding the `unstable` channel.
The Debian package doesn't add any channels at all by default.
This can be verified with:
```sh
nix-channel --list
```

To add the unstable channel to your channels, run the following command:
```sh
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
```

You can probably also add stable channels, but I haven't tried that, as one of the reasons I want to use the Nix package manager on other distros than NixOS is for it's newer packages.
(I am aware that the stable channel of NixOS is a lot newer than the Debian stable branch, which is precisely why I don't want to to be my base system on machines where I have Debian installed.)

## Syncing channels

To sync your (just added) channel(s), run:
```sh
nix-channel --update
```
From this point on you can query your channel(s) using either [the website](https://search.nixos.org/packages) (which is a lot quicker) or:
```sh
nix-env -qa <query>
```
Packages can be imperatively installed using...
```sh
nix-env -iA nixpkgs.<package-name>
```
...and removed using...
```sh
nix-env --uninstall <package-name>
```

Consult [the manual](https://nixos.org/manual/nix/stable/) for further usage.
