---
title: Vim 
date: 2022-03-21T01:17:06+02:00
author: $HUMANOID
tags: ["linux", "vim"]
---

# Vim Cheat sheet

This is a little Vim cheat sheet with thing people generally don't seem to be particularly familiar with when it comes to Vim.
This is about Vim, not NeoVim, but I most things with still apply to NeoVim.

## Self-explanatory things
These are a few bindings that I don't feel require an extensive explanation.

| binding | action |
| --- | --- |
| Z Z | write and quit; equivalent to `:wq` |
| Z Q | quit; equivalent to `:q!` |
| :x | write and quit; equivalent to `:wq` |
| g u | swap characters to lower case |
| g u u | swap characters to lower case on current line |
| g U | swap characters to upper case |
| g U U | swap characters to upper case on current line |
| g {j,k} | if the current line you're on spans more then the width of your screen, go down to the portion that's past the linebreak |
| ~ | toggle case |
| z g | add word to current dictionary |
| [ s | highlight previous misspelled word |
| ] s | highlight next misspelled word |
| z = | open spelling suggestions based on current dictionary and highlighted word |
| . | repeat last modify operation |
| ; | repeat last move operation |
| ! | start command with `:<selection>!` |

## Splits and Terminals
You can split your vim window with somewhat Emacs-like chords by default; I don't bother changing them because I use with quite a few different computers, not all of which have my Vim dotfiles installed.

| binding | action |
| --- | --- |
| C-w s | Horizontal split |
| C-w v | Vertical split |
| C-w {h,j,k,l} | Move focus to split {left,down,up,right} |
| C-w S-{h,j,k,l} | Move split to {left,down,up,right} |

Another usefull thing I don't see a lot of people do is open a terminal session in vim.
This can be really quite useful if you quickly need to change or check something.
You do this with the `:terminal` command.
In Vim, this will horizontally split your current window and open a terminal session in the top half.
(Yes, you can also open Vim sessions in these terminal sessions if you feel like it).
You can navigate to and from it just like any other splits.
In NeoVim terminals are handled significantly worse and is the main reason I went back to base Vim.

## Tips

Something I came across some time ago is that you can interpet the current line with whatever you want using

```
:.!<interpeter>
```

So for instance running...

```
:.!bash
```

...while highlighting the line...

``` bash
echo Line{1..5} | sed 's/ /\n/g'
```

...will result in:

```
Line1
Line2
Line3
Line4
Line5
```

# Configuration

Some settings I like putting in my git configuration files.
```vimrc
set nocompatible
set termguicolors
set cursorline
set hlsearch
set ignorecase
set smartcase
set number
set relativenumber
set wildmenu
set mouse=
set scrolloff=3
set listchars=tab:│\ ,lead:·,trail:·,eol:¬ "'lead' is a NeoVim specific option
set list

" I haven't tried if this works in base vim.
match ExtraWhitespace /\s\+$/
highlight ExtraWhitespace guifg=red
```
