---
title: "Hugo"
date: "2022-08-18T14:53:00+02:00"
author: "$HUMANOID"
tags: ["hugo"]
description: "A little ramble about Hugo"
---

# Reflection
I've tried to take a look at [Hugo](https://gohugo.io) for a few times before properly diving into it.

The first time was back when I first decided to look into making a website not-completely by hand.
I had hacked a few previous iterations of this site together and was happy with the design.
The next step was to write some blobs of text for people to read besides the random antics that can be found in the side bar.

A few problems arose here.
For one, writing directly in HTML is a pain.
So I decided to write in markdown and convert that to HTML using [Pandoc](https://pandoc.org).
From there, I still had to index every page completely by hand.
The prospect of this annoyed me quite a bit, so I decided to take a cursory glance at Hugo. 

The quick start guide showed how to take a pre-existing theme, a bunch of markdown files and turn that into website.
Never having been one to use other people's work as-is, I quickly gave up when I saw the structure of the themes and couldn't be bothered to give it a proper shot.
This also gave me the impression of it being a Wordpress-esque environment that works great as long as you stick within the standards.

A bit later I found out that Pandoc supports templates.
So I tried my hand at hacking a site generator together based on a Makefile and Pandoc.
This worked... somewhat.
But I deemed it more trouble than it was worth and never got very far with it.

A few weeks ago, I came across someone mentioning Hugo again and how it was really great.
I decided to give it another shot.
This time really diving into the structure of the themes and it clicked.
A thing that probably helped this time around was that it very quickly became clear to me that the features I had been half arsedly implementing in my Makefile were standard features for Hugo.
Another thing that undoubtedly helped being that the philosophy and syntax of Pandoc's more advanced features are quite similar to Hugo's.

## Disservice to Hugo

One big thing I bump into with Hugo is the fact that most Hugo websites I come across take one template and _maybe_ change the colorscheme.
I feel this is a great disservice to the potential that Hugo has to offer in terms of flexibility.
Yes, you _can_ have it act similar to Wordpress, but you don't _have to_.
I was scared off because I assumed would have to use some existing template or get my CSS to work within a Hugo context.
The process of which, I assumed, would be mostly having beat Hugo into submission before it would display the things I wanted in the way I wanted.

The beauty of Hugo and ultimately the reason this is now a Hugo site, is it's flexibility.
You wouldn't be able to tell this is a Hugo site if you don't dig through the metadata fields in the `<head>` tag.
And I guess that is also kind of the problem with it.
Most people visibly using Hugo take some standard template proudly proclaiming that the page we're presently looking at is generated using it.

And the people who want to use Hugo but don't care for the pre-existing themes, don't care to proudly shout to the world that their site is made with it. 
This is why I got the impressions of Hugo that I did.

In that sense, I'm part of the problem in not showing that Hugo can do pretty much anything you can think of in the realm of static, somewhat blog oriented websites.
