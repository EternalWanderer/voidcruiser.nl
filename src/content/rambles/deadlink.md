---
title: "Deadlink"
date: "2024-03-24T02:10:59+01:00"
author: "$HUMANOID"
tags: ["Gamedesign", ""]
description: "Deadlink's combat is what Doom Eternal's should be"
draft: true
---

This is a followup of sorts to my [emergent gameplay](/rambles/emergent-gameplay)
ramble.

So the other day, I picked up
[Deadlink](https://store.steampowered.com/app/1676130/Deadlink/), knowing that
its combat was allegedly a lot like Doom Eternal's. With this in mind, I was
pleasantly surprised to find out that Deadlink takes the good parts from Doom
Eternal's combat and builds upon them, while ditching the horrible restrictive
aspects. This means I'm not getting bogged down doing the one thing the games
tells me to do, instead I can happily fly though the arenas, shooting everything
in my path, largely without a care in the world. Or rather, that's how it feels.

Where Deadlink succeeds- and Doom Etenral fails is the implementation of
weakspots. In Eternal, if you want to shoot a weakspot, you don't have much of a
choice but to switch to the machinegun or not-railgunb (I forget its actual
name); those being the only precision weapons. In Deadlink, if you manage to
shoot some poor dude's head with a rocket launcher, the game will still give you
the weakspot damage -- despite normal enemies being able to withstand about a
quarter of a rocket to the feet at most.

Another big mistake that Eternal makes is the tutorialising every restrictive
mechanic that it introduces, while Deadlink lets the player largely figure out
for themselves how the game works. The only things in Deadlink's optional
tutorial are an explanation on basic mechanics -- abilities, c-balls and how
ammo works. The rest is communicated entirely through visual design. Where Doom
Eternal introduces the Arachnotron and halts the game with a message saying that
you can shoot it's gun, Deadlink communicates just as much by having non-head
weakspots subtly pulse orange.
