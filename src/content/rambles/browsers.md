---
title: Browsers
date: 2022-04-04T02:54:33+02:00
author: $HUMANOID
description: "A little ramble mainly about browsers with Vi-like bindings"
tags: ["vim", "linux"]
---

# Web Browsers With Vim Bindings

If there's one thing I like it's Vim bindings everywhere.
Since all Vim browser extentions suck on some level, the remaining choice I have is to use browser with built in vim bindings.
To that end, there are technically a few options.

Those being: 

- [Qutebrowser](https://qutebrowser.org)
- [Vieb](https://vieb.dev)
- [Vimb](https://fanglingsu.github.io/vimb)
- [Nyxt](https://nyxt.atlas.engineer)

These projects are great but all have their problems.
Let's go through them bottom-to-top.

## Nyxt

Nyxt is written in Common Lisp, making it very easy to hack on.
Supports either QtWebEngine or WebKitGTK.
It also appeals more to the Emacs world then the Vim world.
Much like Emacs, Nyxt _does_ have the option of Vim bindings, but they feel somewhat half baked.
Then there is the problem of what little interface there is not being particularly clear.
To be fair, I haven't really given it a proper shot because it's configured on Common fucking Lisp.
Not being much of an Emacs user, I have little to no interest in learning Lisp to configure a browser.

## Vimb

The suckless option.  

Uses WebKitGTK.  
Is written in C and configured in a vimscript-esque syntax.
The thing that turns me off from this browser is it being _too_ suckless.
It almost feels like surf with a slightly more accessible way of configuring it through something that _isn't_ a header file.

Practical reasons that prevent me from using it are the lack of proper ad blocking and tabs.
By default, it also uses the number keys for URL hinting instead of the home row.
It also has similar performance to suckless' surf
In other words, it takes quite a long time to load pages, especially if they contain a large amount of JavaScript (though you shouldn't be visiting those pages unless strictly necessary) and rather quickly hogs quite a lot of resources.

## Vieb

An Electron based browser.

Say whatever you want about Vimb's performance, but Vieb makes Google Chrome seem like a well balanced product.
I've seen it eat 25% of a CPU thread with a single tab containing nothing but the Vieb documentation.

It has a rather pretty interface and could (if the github page is to be believed) be used to interact with other Electron applications like the Discord client as if it were ran natively (don't quote me on that, I'm too lazy to check and probably wrong). 

## Qutebrowser

Which brings us here.  
Qutebrowser is written and configured in Python; and uses the QtWebEngine.
It's extremely flexible and easy to configure.
It has reasonable ad blocking that allows for the addition of blocklists in a plain text file.
A problem I have with it are that I can't choose which JavaScript elements to block and which to allow, like UMatrix.
But then I did see plans to add support for Firefox extensions which would solve this problem.
Another minor gripe I have is that scrolling through long pages can look a little sluggish.

With these changes and (if it weren't a gigantic nightmare) Gecko support, it would be the perfect browser for me.

# Other Web Browsers

When I need to use a site that requires some JS, but still functions without enabling everything, my choice of browser is either Librewolf or a heavily configured Firefox.

From there I use the following list of extensions to make the web usable:

- [vim-vixen](https://github.com/ueokande/vim-vixen)
- [uBlockOrigin](https://github.com/gorhill/uBlock) (Installed by default on Librewolf)
- [redirector](https://github.com/einaregilsson/Redirector)
- [ToS;DR](https://github.com/tosdr/browser-extensions)
