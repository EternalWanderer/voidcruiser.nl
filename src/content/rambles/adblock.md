---
title: "Adblock"
date: "2022-08-22T23:46:09+02:00"
author: "$HUMANOID"
tags: ["internet", "advertisements"]
description: "The morality and utility of adblockers"
---
### UBlock Origin
Before I even get started, I'm going to tell you to use [UBlock Origin](https://ublockorigin.com/).

It is by far the most configurable and extensive adblocker that I know of.
I use it in every browser that supports it.


# Moral Conundrums
You often hear people proclaim that blocking ads is bad for the internet the way that it is.
After which they will say that the alternative is to pay for every now-free aspect of the internet.
Usually through something like subscriptions.

My response is to this is "Have you seen how popular crowd funding has gotten over the last few years?"
People already frequently pay for the content they want to consume.
Sure, it might be less than one might get through ad revenue, but that pays off in not having to shill to daddy {Google,Facebook,Apple,Amazon}.
This in turn means not indirectly violating your user's privacy.

But let's analyse that a bit further.
The claim here is that the lack of advertisements would be bad, because it would mean large portions of the internet would become subscription based in some way.
For one, I'd argue that the presence of ads on the internet has done more harm than good, going as far as saying that, no, the internet would be better off without ads plastered everywhere.
This is never going to happen as there is, understandably a cooperate interest in the internet.
And two, not-insignificant portions of the internet _already are_ subscription based.

{{< hyperbowl content="By blocking ads you are stealing from creators!" >}}

<!--Sometimes you will even come across people who go as far as claiming that blocking ads is theft.-->
Sometimes you will even come across people who go as far to genuinely proclaim the preceding.
To those people, the only thing I can do is quote the classic "You wouldn't download a car, would you?"
Shortly followed by saying "Yes I fucking would, given the opportunity"
No one owes you shit based on the amount of people have ignored the ads on your website.

Then there is also the argument to be made that ads steal one's attention.
The goal here being to manipulate you into buying a product.

Much like with piracy, the you're not missing out on anything.
The only way you would've seen any money from it, would be when you're audience directly sees your content and happens to get an ad shoved in their face.
Though I suppose in the case of ads, your audience has no choice but to support you when they don't use an adblocker.
In other words, you are forcing your audience to sacrifice their a part attention to support you, whether they want to or not.
In my opinion, this makes it morally wrong to make money off of ads.

I'd argue it is a moral imperative to use an adblocker.

Then there is also the angle of corporations focussing on ads more and more.
This in turn means that avoiding an absurd portion of cooperate influence has become hilariously easy;
Just install an adblocker that isn't sponsored by an ad company.
The thing is that most people are too lazy to install an adblocker and thus ads are still a viable business model.

Subscription services such as the one that started as a DVD and VHS rental business and the other one with a monopoly over a significant portion of pop culture, have been working towards ad-supported tiers while drastically increasing the price of their paid tiers.
It would be pretty funny if these ads could be avoided using UBlock Origin.
Personally, I prefer using MPV wherever possible, so I probably won't find out how well that might work.
