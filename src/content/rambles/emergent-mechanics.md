---
title: "Emergent Game Mechanics"
date: "2023-09-25T02:36:11+02:00"
author: "$HUMANOID"
tags: ["gamedesign", "quake", "doom"]
description: "A little piece about emergent mechanics"
---

# Preamble

I usually talk about whatever hobby project I've been hacking together when the
inspiration to write strikes. In a sense this is a similar ramble, just on a
wildly different topic, namely video game design.

# The Meat and Potatoes

For some time now I've been attempting to wrap my head around why I find games
like Doom Eternal to be boring, while games like Ultrakill and Quake are some of
my favourites of all time. And I think I've found the answer.

Doom Eternal has location based damage and various enemy weak spots. For
instance, shooting the Mancubus' arm cannons causes them to be disabled, making
the Mancubus significantly more manageable. Likewise, shooting a grenade at a
Cacodemon causes it swallow the grenade and be stunned for an easy Glory Kill.
The Arachnotron has a turret that can be shot and disabled; the Makyr Drones can
be shot in the head as quick ammo piñatas; the shields on the dudes carrying
them can be easily disabled using the Plasma Rifle, causing a shock wave... so
on and so forth.

Initially, I found these to be really neat ideas. However, it rather quickly
became boring when I found out shooting the weak spots is really the only viable
way to get rid of enemies, especially on higher difficulties. Sure, Doom Eternal
allows you to be very creative in _how_ you shoot those weak spots, but the
moment you stop bothering with them, the difficulty curve starts to resemble
something closer to a vertical line than an actual curve. The end result is that
the combat feels pre-baked and static if you want to be remotely efficient.

Ultrakill does this right. It has -- as far as I'm aware -- two ways the
trajectory of projectiles can be influenced. One being by punching a projectile
as it is about to hit you. The other is with a shock wave, often caused by an
explosion. Shock waves can be caused using the Knuckleduster (heavy melee
attack), the Core Eject (shotgun altfire that shoots a grenade), the rocket
launcher, parrying your own shotgun blast as it exists the barrel and several
that I can't think of right now. This is the only rule regarding projectiles
that is set in stone. Because of this simplicity and how many enemies have
projectile attacks, you can get very creative in how you kill them, while
keeping all approaches equally viable at any given moment. When an enemy has
just shot a projectile at you, you can chose to dodge it, answer it with a
shotgun blast; punch it back to the culprit using a light melee attack; switch
to heavy melee and shoot it back using a shock wave; overcharge the pump action
shotgun, dodge out of its explosion damage using the I-frames received when
dodging, while using said same explosion to send the projectile back to the
unsuspecting enemy; use those I-frames from the dodge to go through the
projectile and punch the enemy in the face retrieving some health in the
process, etc, etc.

All of these are viable stratagies depending on how much health you still have
and how many other enemies there are. Getting out of the way of overcharge
explosion damage by dodging is no small feat, but might save you from more
damage if you're surrounded by a lot of high level enemies. Punching a single
enemy isn't something you'll want to do when still surrounded as it means doing
less damage in that moment and only damaging a single enemy. I could go on and
on, describing how and when one might chose any given strategy, but I'll try to
stop here.

The result is an incredibly dynamic combat system that allows for endless
creativity in dealing with any given scenario. If Ultrakill's design philosophy
were closer to that of Doom Eternal, there would be a dedicated "redirect
projectile" button/weapon. It wouldn't serve much of a purpose outside of
deflecting the occasional projectile. On top of that, each enemy attack would
have one and only one viable way to answer it.

For me an encounter in Doom Eternal would go something like the following:

> Mancubus, take out its cannons using machine gun altfire; Arachnotron, ditto;
> Cacodemon, switch to shotgun, shoot grenade using altfire, Glory Kill; "oh
> hey, shield dudes", switch to Plasma Rifle and pepper them a bit...

There is (usually) one "proper" strategy to deal with each enemy. As a result,
instead of improvising throughout the entire fight, I'm focussing on what hoops
to jump through and in what order. And I guess I don't like being treated as a
circus lion. In Ultrakill, I just do what comes natural in any given situation
without feeling like I'm being forced down a rather
face-paced-yet-impressively-boring path.

Doom Eternal's movement is even more strict. There is a double jump, double dash
and later on a grapple hook. Sure, there are some interesting things you can do
here, but short of exploiting the physics engine by manipulating the framerate
by opening the weapon wheel, you're not going to see any crazy movement
strategies.

Ultrakill has a triple dash, a slide, dash jump, ground pound and the player
character can be knocked about by explosions and shock waves. On top of that,
there is also the very intricate interplay between these elements due to how the
physics engine works.

The result here is a set of extremely complex mechanics that are never set in
stone; instead emerging from a few laws of the game that don't have total death
grip control over their domain.

There is no way I could write an article about emergent mechanics without
mentioning Quake. So lets do that now.

None of the high level mechanics in Quake are intended. Everything from
straferunning, to wallrunning, to rocket jumping, to bunnyhopping and power
bunnyhopping (also called strafejumping in the context of Quake) is the result
of a few basic rules and wack physics.

1. "The player can turn"
2. "The player can move in 8 cardinal directions"
3. "The player can jump"
4. "The player can sprint, increasing their speed"
5. "There are entities that are affected by physics"
6. "The player is one such entity"

And that's it as far as I'm aware.

All the of the complex movement mechanics emerge from the interplay of these
rules. One of the first that was found was straferunning. What this means is
sending both a move-forward and strafe input. Due to the way velocity is
calculated, this results in a slight speed increase. Shortly after this, it was
discovered that doing this while running into a wall causes a greater speed
increase. As did wiggling the mouse left and right. When jumping you don't lose
any momentum. Put all of these together -- with the exception of wallrunning --
add little finesse on top and the result is strafejumping.

To me there is something about using these emergent mechanics that is infinitely
more satisfying than the pre baked equivalents in modern games. Figuring out how
to (ab)use the game's rules to your advantage is half the fun if you ask me and
games like Doom Eternal take most of that away from you by giving you one or
very few viable strategies to approach a given situation and then telling you
most of them from the get go.
